package com.example.clevertek_ex_2

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.clevertek_ex_2.databinding.ElemSecondBinding

class Elem_second:Fragment() {

    private lateinit var binding: ElemSecondBinding

    override fun onCreateView(inflater: LayoutInflater, container:ViewGroup?, savedInstantState: Bundle?):View {
        binding = ElemSecondBinding.inflate(inflater)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val title : TextView = view.findViewById(R.id.textV2)
        val description : TextView = view.findViewById(R.id.textV1)
        val titleText = arguments?.getString("Tittle")
        val descriptionText = arguments?.getString("Descr")
        title.text = titleText
        description.text = descriptionText
    }
}