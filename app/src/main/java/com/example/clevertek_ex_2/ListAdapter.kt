package com.example.clevertek_ex_2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.clevertek_ex_2.databinding.ElemBinding

class ListAdapter:RecyclerView.Adapter<ListAdapter.MyHolder>(){

    val MyList = ArrayList<ElemForRV>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.elem,parent,false)
        return MyHolder(view,mListener)
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {
        holder.bind(MyList[position])
    }

    override fun getItemCount(): Int {
        return MyList.size
    }

    fun addItem(item: ElemForRV){
        MyList.add(item)
        notifyDataSetChanged()
    }

    class MyHolder(item: View,listener: onMyHolderActList):RecyclerView.ViewHolder(item){
        val binding = ElemBinding.bind(item)
        fun bind (MyCont:ElemForRV) = with(binding){
            textView.text = MyCont.title
            textView1.text = MyCont.description
        }

        init {
            itemView.setOnClickListener {
                val MyTitle = binding.textView.text.toString()
                val MyDescription = binding.textView1.text.toString()
                listener.onItemListClick(MyTitle,MyDescription)
            }
        }
    }

    private lateinit var mListener: onMyHolderActList
    interface onMyHolderActList{
        fun onItemListClick(title: String,description: String)
    }

    fun setOnItemActionListener(listener: onMyHolderActList){
        mListener = listener
    }
}