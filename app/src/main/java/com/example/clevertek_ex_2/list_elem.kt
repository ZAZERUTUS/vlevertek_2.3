package com.example.clevertek_ex_2

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.clevertek_ex_2.databinding.ListElemBinding

class list_elem : Fragment() {

    companion object {
        fun newInstance() = list_elem()
    }
    lateinit var binding: ListElemBinding
    lateinit var adapter: ListAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = ListElemBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        adapter = ListAdapter()
        adapter.setOnItemActionListener(object : ListAdapter.onMyHolderActList{
            override fun onItemListClick(title: String, description: String) {
                val bundle = Bundle()
                bundle.putString("Tittle",title)
                bundle.putString("Descr",description)
                if(requireActivity() is MainActivity){
                    findNavController().navigate(R.id.action_list_elem_to_elem_second,bundle)
                }

            }

        })

        binding.listRC.layoutManager = LinearLayoutManager(context)
        binding.listRC.adapter = adapter
        for(i in 1..1000){
            val item = ElemForRV("Title$i","Description $i")
            adapter.addItem(item)

        }

    }
}
